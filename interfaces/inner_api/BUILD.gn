# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/distributeddatamgr/data_share/datashare.gni")
import("//foundation/distributeddatamgr/relational_store/relational_store.gni")

config("ability_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "common/include",
    "consumer/include",
    "provider/include",
    "${ability_runtime_services_path}/common/include",
    "${datashare_native_consumer_path}/include",
    "${datashare_native_provider_path}/include",
    "${datashare_native_proxy_path}/include",
    "${ability_runtime_napi_path}/inner/napi_common",
  ]

  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
}

config("datashare_public_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "common/include",
    "consumer/include",
    "provider/include",
    "${datashare_common_napi_path}/include",
    "${datashare_common_native_path}/include",
    "${datashare_native_consumer_path}/include",
    "${datashare_native_provider_path}/include",
    "${datashare_native_proxy_path}/include",
  ]
}

ohos_shared_library("datashare_consumer") {
  include_dirs =
      [ "//foundation/distributeddatamgr/kv_store/frameworks/common" ]

  sources = [
    "${datashare_native_consumer_path}/src/connection_factory.cpp",
    "${datashare_native_consumer_path}/src/datashare_connection.cpp",
    "${datashare_native_consumer_path}/src/datashare_helper.cpp",
    "${datashare_native_consumer_path}/src/datashare_proxy.cpp",
    "${datashare_native_proxy_path}/src/data_proxy_observer_stub.cpp",
    "${datashare_native_proxy_path}/src/data_share_manager_impl.cpp",
    "${datashare_native_proxy_path}/src/data_share_service_proxy.cpp",
    "${datashare_native_proxy_path}/src/published_data_subscriber_manager.cpp",
    "${datashare_native_proxy_path}/src/rdb_subscriber_manager.cpp",
  ]
  configs = [ ":ability_config" ]
  public_configs = [ ":datashare_public_config" ]
  version_script = "consumer/libdatashare_consumer.map"
  innerapi_tags = [ "platformsdk" ]
  deps = [ "${datashare_innerapi_path}/common:datashare_common" ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_manager",
    "ability_runtime:app_context",
    "ability_runtime:dataobs_manager",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
    "ipc_js:rpc",
    "napi:ace_napi",
    "samgr:samgr_proxy",
  ]

  subsystem_name = "distributeddatamgr"
  part_name = "data_share"
}

ohos_shared_library("datashare_provider") {
  include_dirs =
      [ "//foundation/distributeddatamgr/kv_store/frameworks/common" ]

  sources = [
    "${datashare_native_provider_path}/src/datashare_ext_ability.cpp",
    "${datashare_native_provider_path}/src/datashare_ext_ability_context.cpp",
    "${datashare_native_provider_path}/src/datashare_stub.cpp",
    "${datashare_native_provider_path}/src/datashare_stub_impl.cpp",
    "${datashare_native_provider_path}/src/datashare_uv_queue.cpp",
    "${datashare_native_provider_path}/src/js_datashare_ext_ability.cpp",
    "${datashare_native_provider_path}/src/js_datashare_ext_ability_context.cpp",
  ]
  configs = [ ":ability_config" ]
  public_configs = [ ":datashare_public_config" ]
  version_script = "provider/libdatashare_provider.map"
  innerapi_tags = [ "platformsdk" ]
  deps = [ "${datashare_innerapi_path}/common:datashare_common" ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_context_native",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_context",
    "ability_runtime:dataobs_manager",
    "ability_runtime:runtime",
    "access_token:libaccesstoken_sdk",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
    "ipc_js:rpc",
    "napi:ace_napi",
  ]

  subsystem_name = "distributeddatamgr"
  part_name = "data_share"
}

ohos_shared_library("datashare_ext_ability_module") {
  include_dirs = [ "${datashare_native_provider_path}/include" ]

  sources = [ "${datashare_native_provider_path}/src/datashare_ext_ability_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":datashare_public_config" ]

  deps = [ ":datashare_provider" ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:abilitykit_native",
    "ability_runtime:runtime",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "hiviewdfx_hilog_native:libhilog",
  ]

  relative_install_dir = "extensionability/"
  subsystem_name = "distributeddatamgr"
  part_name = "data_share"
}
